#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>
#include <spring\Application\MonoInput.h>
#include <qaudioinput.h>
#include "SerialPort.h"
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

  public slots:
      void mp_BackButton();
	  void mf_PlotRandom();
	  void mf_CleanPlot();

	  void mf_StartTimer();
	  void mf_StopTimer();
	
  private:
		QWidget * centralWidget;
		QCustomPlot* mv_customPlot;
		QTimer mv_timer;
		double mv_dfRefreshRate;
		char* portName;
		SerialPort *arduino;
		MonoInput* mMonoInput;
		QAudioInput* mAudioInput = nullptr;

		void mp_InitPlot();
		void mp_InitMicrophone();
		void mp_StopMicrophone();
		char incomingData[MAX_DATA_LENGTH];
		void mp_StopArduino();
	};
}
