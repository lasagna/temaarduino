#include <QIODevice>
#include <QAudioFormat>
#include <qvector.h>

class MonoInput : public QIODevice
{
public:
	MonoInput(
		const double aSampleRate
		, const int nNoOfChannels = 1
		, const int nSampleSize = 16
		, const QAudioFormat::SampleType eSampleType = QAudioFormat::SignedInt
		, const QAudioFormat::Endian eEndiannes = QAudioFormat::LittleEndian
		, const QString& sCodec = "audio/pcm");

	~MonoInput();

	qint64 readData(char* data, qint64 maxlen);
	qint64 writeData(const char *data, qint64 len);

	QAudioFormat getAudioFormat();
	QVector<double> vecGetData();

private:
	QAudioFormat mAudioFormat;
	QVector<double> mSamples;
	qint32 mMaxAmplitude;
};

